<?php

namespace Drupal\post\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;

/**
 * @Block(
 *   id = "make_proposals_block",
 *   admin_label = @Translation("Block with make Proposals button"),
 * )
 */
class AddProposalBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   *
   * @param EntityFormBuilderInterface $entityFormBuilder
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }


  /**
   * Method for build block.
   *
   * @return array
   */
  public function build() {
    $node = \Drupal::routeMatch()->getParameter('node');
    $status = $this->checkAccess($node);

    switch ($status) {
      case 'process' :
        $build['make_proposal'] = [
          '#markup' => "Customer have checked proposition. The project is already processed.",
        ];
        break;

      case 'complete' :
        $build['make_proposal'] = [
          '#markup' => "Project is already finished",
        ];
        break;

      case 'filled' :
        $build['make_proposal'] = [
          '#markup' => "You've already submitted a proposal for this project",
        ];
        break;

      default:
        $build['make_proposal'] = [
          '#type' => 'button',
          '#value' => new TranslatableMarkup('Make a Proposal'),
          '#name' => new TranslatableMarkup('makeProposal'),
          '#weight' => 0,
          '#attributes' => [
            'class' => ['btn', 'btn-success'],
          ],
        ];
    }

    return $build;
  }

  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowed();
  }

  protected function checkAccess($node) {
    if ($node instanceof NodeInterface) {

      $nid = $node->id();
      $comments = \Drupal::entityTypeManager()
        ->getStorage('comment')
        ->loadByProperties(['entity_id' => $nid]);
      if (!empty($comments)) {
        foreach ($comments as $comment) {
          if ($comment->getOwnerId() == \Drupal::currentUser()->id()) {
            return 'filled';
          }
        }
      }

      return $node->get("field_state")->getString();
    }
  }

}
