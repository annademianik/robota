<?php

namespace Drupal\post\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;

/**
 * @Block(
 *   id = "choose_proposals_block",
 *   admin_label = @Translation("Block with choose Proposals button"),
 * )
 */
class ChooseProposalBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   *
   * @param EntityFormBuilderInterface $entityFormBuilder
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }


  /**
   * Method for build block.
   *
   * @return array
   */
  public function build() {
    // Attach the library for pop-up dialogs/modals.
    $build['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $build['choose_proposal'] = [
      '#type' => 'link',
      '#title' => $this->t('Choose Proposal'),
      '#url' => Url::fromRoute('post.open_modal_form'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button',
          'btn',
          'btn-success',
        ],
      ],
    ];

    return $build;
  }

  protected function blockAccess(AccountInterface $account) {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof NodeInterface) {
      if ($node->getOwnerId() == $account->id()) {
        return AccessResult::allowed();
      }
    }
    return AccessResult::forbidden();
  }

}
