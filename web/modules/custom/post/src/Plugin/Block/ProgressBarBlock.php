<?php

namespace Drupal\post\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;

/**
 * @Block(
 *   id = "booking_pricing_detail",
 *   admin_label = @Translation("Booking Pricing Detail"),
 * )
 */
class ProgressBarBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var EntityFormBuilderInterface
   */

  /**
   *
   * @param EntityFormBuilderInterface $entityFormBuilder
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }


  /**
   * Method for build block.
   *
   * @return array
   */
  public function build() {
    $booking = \Drupal::routeMatch()
      ->getParameter('bat_booking'); //get booking

    $space = $booking->get('field_space_id')->getValue()[0]['target_id'];

    $booking_end = $booking->get('booking_end_date')->getValue()[0]['value'];
    $booking_start = $booking->get('booking_start_date')
      ->getValue()[0]['value'];
    $space = UnitType::load($space);
    $cost = \Drupal::service('way_space.booking_pricing')
      ->calculatePrice($space, $booking_start, $booking_end);

    $spaceDefaultPrice = $space->get('field_default_price')
      ->getValue()[0];
    $spaceDefaultPrice['number'] = number_format($spaceDefaultPrice['number'], 2, '.', '');
    $price = $cost['price'];
    $detail = $cost['priceDetail'];
    $output_result = [
      '#theme' => 'block_booking_detail',
      '#summPrice' => $price,
      '#spaceDefaultPrice' => $spaceDefaultPrice,
      '#summDuration' => $detail['amount'],
      '#periodItem' => $detail['output_data'],
      '#spacePeriod' => $cost["priceDetail"]["duration"]["period"],
    ];
    return $output_result;
  }

}
