<?php

namespace Drupal\post\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * ModalForm class.
 */
class ChooseProposalForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'modal_form_example_modal_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {
    $form['#prefix'] = '<div id="modal_example_form">';
    $form['#suffix'] = '</div>';
//    $node = \Drupal::routeMatch()->getParameter('node');
//    $nid = $node->id();
    $comments = \Drupal::entityTypeManager()
      ->getStorage('comment')
      ->loadByProperties(['entity_id' => $options["nid"]]);
    $form['nid'] = $options["nid"];
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('comment');
    foreach ($comments as $cid => $comment) {
      $pre_render = $view_builder->view($comment, 'default');
      $render_output = render($pre_render);
      $aa[$cid] = $render_output;
    }

    $form['select'] = [
      '#type' => 'radios',
//      '#title' => t('Preview comment'),
//      '#options' => ['d' => '<div>dscds</div>'],
      '#options' => $aa,
//      '#options' => $comments,
      ];

      //    $view = \Drupal\views\Views::getView('user_proposals');
//    $render_array = $view->buildRenderable('entity_browser', ['cardinality' => 1]);
//    $form["rendered"] = [
//      '#markup' => \Drupal::service('renderer')->renderRoot($render_array),
//    ];
    // The status messages that will contain any form errors.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit modal form'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#modal_example_form', $form));
    }
    else {
      //todo
      $node = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadByProperties(['nid' =>  $form['nid']]);
      $node = array_shift($node);
      $node->set("field_state", 'process');
      $node->save();
      $response->addCommand(new OpenModalDialogCommand("Success!", 'The modal form has been submitted.', ['width' => 800]));
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['config.modal_form_example_modal_form'];
  }

}
