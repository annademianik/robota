<?php

namespace Drupal\cu_user\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An example controller.
 */
class UserPortfolioController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content($user) {
    $form = \Drupal::entityTypeManager()
      ->getFormObject('user', 'portfolio')
      ->setEntity($user);

    return \Drupal::formBuilder()->getForm($form);
  }

}
