<?php

namespace Drupal\cu_user\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An example controller.
 */
class UserSettingsController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content($user) {
    $form = \Drupal::entityTypeManager()
      ->getFormObject('user', 'account_settings')
      ->setEntity($user);

    return \Drupal::formBuilder()->getForm($form);
  }

}
