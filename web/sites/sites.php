<?php

$sites['default'] = 'default';
$sites['china'] = 'china';
// If we're on Tugboat, use the unique URL token that Tugboat provides to also
// map Tugboat's aliased URLs to the correct site.
if ($tugboat_token = getenv('TUGBOAT_DEFAULT_SERVICE_TOKEN')) {
  $sites["default-{$tugboat_token}.tugboat.qa"] = 'default';
  $sites["china-{$tugboat_token}.tugboat.qa"] = 'china';
}

// Map the main Preview URL to arithmancy.
$sites[getenv('TUGBOAT_DEFAULT_SERVICE_URL_HOST')] = 'default';

