(function ($) {
    "use strict";

    Drupal.behaviors.proposalsForm = {
      attach: function (context, settings) {
        var makeProposal = document.getElementsByName('makeProposal');
        makeProposal[0].addEventListener("click", showForm);

        function showForm() {
          var formProposal = document.getElementById('makeProposalForm');
          formProposal.style.display = 'block';
          makeProposal[0].style.display = 'none';
        }

      }
    };

  }
  (jQuery)

);
